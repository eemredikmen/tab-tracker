const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const dotenv = require('dotenv');

dotenv.config({ path: './config/.env' })

const app = express();
const PORT = process.env.PORT || 8081;

//* json parser

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(morgan('combined'));
app.use(cors());

// test router

app.get('/test', (req, res) => {
  res.json({
    messsage: 'Welcome mevn stack project'
  })
})

app.post('/register', (req, res) => {
  res.json({
    message: 'user was registered'
  })
  console.log(req.body);
})

app.listen(PORT, () => {
  console.log(`Server runnig on : ${PORT}`);
})